const {response,request} =require('express');
const {ConvertirFechaF,ConvertirFechaBD}=require('../helpers/validar-fecha');
const Producto = require('../models/producto'); 



const productoGet = async (req=request, res=response) => {

    const {createdAt} = req.params;
    try {
        const fechaFormateada=ConvertirFechaBD(createdAt);
        const producto = await Producto.find({
            createdAt:{$gte:fechaFormateada}
        }).
        then(listado=>res.json(listado));
        
    } catch (error) {
        return res.status(500).json(error);
    }
    

    
    
}

const productoPost = async (req=request, res=response) => { 
    
    
    const {name,price,createdAt,... body} = req.body; 
 
    //Guardar en bd
    try {
        let created=ConvertirFechaBD(createdAt);
        const producto =Producto({name,price,'createdAt':created});//ya tengo la instancia

        await producto.save();
        return res.status(201).json({
            producto
        });     
    } catch (error) {
        return res.status(500).json(error);
    }

    
}



module.exports={
    productoGet,
    productoPost
}
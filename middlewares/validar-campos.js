const { validationResult } = require('express-validator');


const validarCampos = (req,res,next) => {
  
    const errors = validationResult(req); //obtenemos los errores que detectó el middleware
    if (!errors.isEmpty()){ //si hay errores, retornamos el error
        return res.status(400).json(errors);
    }
    next();//si llega a este punto, debe seguir con el siguiente middleware
}

module.exports={
    validarCampos
}
const express = require('express');
const {dbConnection} = require('../db/conexion');
class Server{
    constructor(){

        this.app =express();
        this.port=process.env.PORT||8080;//tengo acceso por ser variable global

        this.paths = {
            productos:'/productos',
        };
        //conectar a BD
        this.conectarBD();

        //middlewares
        this.middlewares();
        //rutas de mi app
        this.routes(); 
        
    }
    async conectarBD(){
        await dbConnection();
    }
    middlewares(){
        //Lectura y parseo del body
        this.app.use(express.json());
    
    }
    routes(){
        this.app.use(this.paths.productos,require('../routes/productos'));
    }
    listen(){
        this.app.listen(this.port,() => { 
            console.log('Servidor corriendo en: ',this.port);
        });
    }
}

module.exports=Server;
const {Schema,model} = require('mongoose');
const ProductoSchema = Schema({
    name: {
        type: String,
        unique:true,
        required: [true,'El nombre es obligatorio']
    },
    state:{
        type:Boolean,
        default:true
    },
    price:{
        type: Number,
        default:0,
    },
    createdAt:{
        type:Date
    }
});


ProductoSchema.methods.toJSON = function (){ //le digo que quite los datos que reenvía
    const {__v, state, ... data } = this.toObject();
    return data;
}

module.exports = model('Producto',ProductoSchema);
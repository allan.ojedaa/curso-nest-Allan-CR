const {Router } = require('express');
const { check } = require('express-validator');
const {
        productoGet,productoPost 
    } = require('../controllers/productos');

const {validarCampos} =require('../middlewares/validar-campos')

const router = Router();


//ejemplo localhost:8080/productos/29-03-2021
router.get('/:createdAt', productoGet );  


router.post('/',[ //validaciones
    check('name','El nombre es obligatorio').not().isEmpty(), //
    check('price','El precio debe ser un número positivo').isFloat({min:0}),
    check('createdAt','La fecha no es válida').not().isEmpty(),
    validarCampos], productoPost);  

 

module.exports=router; //configuraré las rutas
const mongoose = require('mongoose')


const dbConnection = async () => {
  try {
      const url='mongodb+srv://'+process.env.USER+':'+process.env.PASS+'@cluster0.vicxa.mongodb.net/'+process.env.DB;
      
      console.log(url);
      await mongoose.connect(url,{
          useNewUrlParser:true,
          useUnifiedTopology:true,
          useCreateIndex:true,
          useFindAndModify:false
      });
      console.log('Base de datos conectada');
  } catch (error) {
      console.log(error);
      throw new Error('Error en la inicialización de la base de datos');
  }

}

module.exports = {
    dbConnection
}
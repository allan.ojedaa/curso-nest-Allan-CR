const moment = require('moment');



const ConvertirFechaBD= (fecha="")=>{
    const IsoDateTo = moment(fecha,'DD/MM/YYYY').format('YYYY-MM-DD');
    return IsoDateTo.toString();
}
const ConvertirFechaF= (fecha="")=>{
    const IsoDateTo = moment(fecha,'YYYY-MM-DD').format('DD/MM/YYYY');
    return IsoDateTo.toString();
}

module.exports={ConvertirFechaF,ConvertirFechaBD}